package shivam.paddingvalues;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView t;
    RatingBar rb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t= (TextView) findViewById(R.id.tv);
        rb = (RatingBar) findViewById(R.id.rb);

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(rating==1.0f)
                {
                    t.setText("Poor");
                }
                else if (rating==2.0f)
                {
                    t.setText("Below Average");
                }
                else if (rating==3.0f)
                {
                    t.setText("Average");
                }
                else if (rating==4.0f)
                {
                    t.setText("Better");
                }
                else if (rating==5.0f)
                {
                    t.setText("Good");
                }
            }
        });
    }
}
